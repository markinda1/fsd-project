import { Token } from '@prisma/client';

export interface Tokens {
  accessToken: string;
  refreshToken: Token;
}

export interface JwtPayload {
  id: number;
  email: string;
  role: string;
  name: string;
}

export interface UserReturn {
  id: number;
  email: string;
  role: string;
  name: string;
}
