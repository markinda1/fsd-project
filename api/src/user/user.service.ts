import {
  ConflictException,
  ForbiddenException,
  Injectable,
  Logger,
} from '@nestjs/common';
import { PrismaService } from '@prisma/prisma.service';
import { Role } from '@prisma/client';
import { genSaltSync, hashSync } from 'bcrypt';
import { JwtPayload } from '@auth/interfaces';
import { RegisterDto } from '@user/dto';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);
  constructor(private readonly prismaService: PrismaService) {}

  async save(dto: RegisterDto) {
    const password = this.hashPassword(dto.password);

    const userFind = await this.searchEmail(dto.email).catch((err) => {
      this.logger.error(err);
    });

    if (userFind) {
      throw new ConflictException(
        'A user with this email is already registered',
      );
    }

    return this.prismaService.user.create({
      data: {
        email: dto.email,
        password: password,
        role: 'VIEW',
        name: dto.name,
      },
    });
  }

  async findOne(id: number) {
    const user = await this.prismaService.user.findFirst({
      where: {
        id,
      },
    });

    if (!user) {
      return null;
    }

    return {
      id: user.id,
      email: user.email,
      role: user.role,
      name: user.name,
    };
  }

  async searchEmail(email: string) {
    const user = await this.prismaService.user.findFirst({
      where: {
        email,
      },
    });

    if (!user) {
      return null;
    }

    return user;
  }

  async delete(id: number, user: JwtPayload) {
    if (user.id !== id && user.role !== Role.ADMIN) {
      throw new ForbiddenException();
    }
    return this.prismaService.user.delete({
      where: { id },
      select: { id: true },
    });
  }

  private hashPassword(password: string) {
    return hashSync(password, genSaltSync(10));
  }
}
