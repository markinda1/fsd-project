import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
} from '@nestjs/common';
import { UserService } from '@user/user.service';
import { JwtPayload } from '@auth/interfaces';
import { CurrentUser, Public } from '@shared/decorators';
import { RegisterDto } from '@auth/dto';
import { User } from '@prisma/client';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Post('')
  async create(@Body() body: RegisterDto) {
    const user: Partial<User> = await this.userService.save(body);

    if (!user) {
      throw new BadRequestException(
        'Не получилось зарегистрировать пользователя!',
      );
    }

    return {
      data: {
        id: user.id,
        email: user.email,
        role: user.role,
        name: user.name,
      },
    };
  }

  @Get(':id')
  async findOneUser(@Param('id') id: number) {
    const user = await this.userService.findOne(Number(id));

    return {
      id: user.id,
      email: user.email,
      role: user.role,
      name: user.name,
    };
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: number, @CurrentUser() user: JwtPayload) {
    return this.userService.delete(Number(id), user);
  }
}
