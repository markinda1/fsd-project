import './layout.scss';
import clsx from 'clsx';
import { Outlet } from 'react-router-dom';
import { Header } from '@/widgets/header';

export const AppLauout = () => {
  return (
    <>
      <Header />
      <main className={clsx('app-layout')}>
        <Outlet />
      </main>
    </>
  );
};
