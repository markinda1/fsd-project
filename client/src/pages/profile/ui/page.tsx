import './page.scss';
import { Avatar, Typography } from 'antd';
import clsx from 'clsx';
import { useViewer } from '@/entities/viewer';
import { userNoPhoto } from '@/shared/lib';

const { Text } = Typography;

export const ProfilePage = () => {
  const viewer = useViewer();

  return (
    <section className={clsx('profile-page')}>
      <div className={clsx('container')}>
        <div className={clsx('profile-page__header')}>
          <Avatar size={100} className={clsx('profile-page__avatar')}>
            {userNoPhoto(viewer?.auth.name)}
          </Avatar>
          <Text className={clsx('profile-page__name')}>{viewer?.auth.name}</Text>
        </div>
      </div>
    </section>
  );
};
