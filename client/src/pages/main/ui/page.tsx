import './page.scss';
import { Typography } from 'antd';
import clsx from 'clsx';

const { Title } = Typography;

export const MainPage = () => {
  return (
    <section className={clsx('main-page')}>
      <div className={clsx('container')}>
        <Title>Main Page</Title>
      </div>
    </section>
  );
};
