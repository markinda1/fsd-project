import './page.scss';
import clsx from 'clsx';
import { LogInForm } from '@/features/auth';

export const LoginPage = () => {
  return (
    <section className={clsx('login-page')}>
      <div className={clsx('container')}>
        <LogInForm />
      </div>
    </section>
  );
};
