import './page.scss';
import { Button, Typography } from 'antd';
import clsx from 'clsx';
import { useNavigate } from 'react-router-dom';
import { navigationMap } from '@/shared/model';

const { Title, Paragraph } = Typography;

export const NotFoundPage = () => {
  const navigate = useNavigate();

  return (
    <section className={clsx('not-found-page')}>
      <div className={clsx('container')}>
        <Title className={clsx('not-found-page__title')}>404</Title>
        <Paragraph>Sorry, this page does not exist</Paragraph>
        <Button onClick={() => navigate(navigationMap.main)}>To main</Button>
      </div>
    </section>
  );
};
