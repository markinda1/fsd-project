import './page.scss';
import clsx from 'clsx';
import { RegisterForm } from '@/features/auth';

export const RegisterPage = () => {
  return (
    <section className={clsx('register-page')}>
      <div className={clsx('container')}>
        <RegisterForm />
      </div>
    </section>
  );
};
