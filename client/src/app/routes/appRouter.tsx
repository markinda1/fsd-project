import React from 'react';
import { unstable_HistoryRouter as HistoryRouter, Navigate, Outlet, Route, Routes } from 'react-router-dom';
import { useAppDispatch } from '@/app/appStore.ts';
import { AppLauout } from '@/pages/layouts';
import { LoginPage } from '@/pages/login';
import { MainPage } from '@/pages/main';
import { NotFoundPage } from '@/pages/not-found';
import { ProfilePage } from '@/pages/profile';
import { RegisterPage } from '@/pages/register';
import { getMe, useAuth } from '@/entities/viewer';
import { appHistory, removeToken } from '@/shared/lib';
import { navigationMap } from '@/shared/model';

export const AppRouter = () => {
  const dispatch = useAppDispatch();
  const isAuth = useAuth();

  const accessToken = localStorage.getItem('accessToken');

  React.useEffect(() => {
    if (accessToken) {
      dispatch(getMe(accessToken));
    } else {
      removeToken();
    }
  }, [accessToken, dispatch]);

  return (
    <HistoryRouter history={appHistory}>
      <Routes>
        <Route element={<AppLauout />}>
          <Route path={navigationMap.main} element={<MainPage />} />
          <Route element={isAuth || accessToken ? <Navigate to={navigationMap.profile} /> : <Outlet />}>
            <Route path={navigationMap.login} element={<LoginPage />} />
            <Route path={navigationMap.register} element={<RegisterPage />} />
          </Route>
          <Route element={isAuth || accessToken ? <Outlet /> : <Navigate to={navigationMap.login} />}>
            <Route path={navigationMap.profile} element={<ProfilePage />} />
          </Route>
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
    </HistoryRouter>
  );
};
