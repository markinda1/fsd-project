import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { rootReduce } from '@/app/appReducer.ts';

const PRODUCTION_MODE = 'production';

const appStore = configureStore({
  reducer: rootReduce,
  middleware: getDefaultMiddleware => getDefaultMiddleware(),
  devTools: import.meta.env.VITE_DEV_MODE !== PRODUCTION_MODE,
});

export default appStore;

export type RootState = ReturnType<typeof appStore.getState>;

export type AppDispatch = typeof appStore.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
