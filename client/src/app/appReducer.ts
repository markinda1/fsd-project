import { combineReducers } from '@reduxjs/toolkit';
import { usersSlice } from '@/entities/user';
import { viewerSlice } from '@/entities/viewer';

export const rootReduce = combineReducers({
  viewer: viewerSlice.reducer,
  users: usersSlice.reducer,
});
