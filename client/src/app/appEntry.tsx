import '@/shared/ui/index.scss';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider as ReduxProvider } from 'react-redux';
import { AppRouter } from '@/app/routes';
import appStore from './appStore.ts';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ReduxProvider store={appStore}>
      <AppRouter />
    </ReduxProvider>
  </React.StrictMode>,
);
