import './index.scss';
import { Button, Input, Typography } from 'antd';
import clsx from 'clsx';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '@/app/appStore.ts';
import { userCreate, useUsers } from '@/entities/user';
import { navigationMap } from '@/shared/model';

const { Link } = Typography;

export const RegisterForm = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const users = useUsers();
  const [email, setEmail] = React.useState<string>('');
  const [password, setPassword] = React.useState<string>('');
  const [passwordRepeat, setPasswordRepeat] = React.useState<string>('');
  const [name, setName] = React.useState<string>('');

  const handlerSignUp = React.useCallback(() => {
    dispatch(
      userCreate({
        email: email,
        name: name,
        password: password,
        passwordRepeat: passwordRepeat,
      }),
    );
  }, [dispatch, email, password, name, passwordRepeat]);

  return (
    <div className={clsx('register-form')}>
      {users?.errors?.message}
      <Input
        value={email}
        placeholder="Email"
        onChange={event => {
          setEmail(event.target.value);
        }}
        name="email"
        status={users?.errors?.email ? 'error' : ''}
        autoFocus={true}
      />
      <Input
        value={name}
        placeholder="Name"
        onChange={event => {
          setName(event.target.value);
        }}
        name="name"
        status={users?.errors?.name ? 'error' : ''}
      />
      <Input.Password
        value={password}
        placeholder="Password"
        onChange={event => {
          setPassword(event.target.value);
        }}
        name="password"
        status={users?.errors?.password ? 'error' : ''}
      />
      <Input.Password
        value={passwordRepeat}
        placeholder="Password Repeat"
        onChange={event => {
          setPasswordRepeat(event.target.value);
        }}
        name="passwordRepeat"
        status={users?.errors?.passwordRepeat ? 'error' : ''}
      />
      <Button
        type="primary"
        onClick={handlerSignUp}
        disabled={users?.loading || !email || !name || !password || !passwordRepeat}
        loading={users?.loading}
      >
        Sign Up
      </Button>
      <Link onClick={() => navigate(navigationMap.login)}>Already have an account?</Link>
    </div>
  );
};
