import './index.scss';
import { Input, Button, Typography } from 'antd';
import clsx from 'clsx';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '@/app/appStore.ts';
import { signIn, useViewer } from '@/entities/viewer';
import { navigationMap } from '@/shared/model';

const { Link } = Typography;

export const LogInForm = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const viewer = useViewer();
  const [email, setEmail] = React.useState<string>('');
  const [password, setPassword] = React.useState<string>('');

  const handlerSignIn = React.useCallback(() => {
    dispatch(
      signIn({
        email: email,
        password: password,
      }),
    );
  }, [dispatch, email, password]);

  return (
    <div className={clsx('log-in-form')}>
      {viewer?.errors?.message}
      <Input
        value={email}
        placeholder="Email"
        onChange={event => {
          setEmail(event.target.value);
        }}
        name="email"
        status={viewer?.errors?.email ? 'error' : ''}
        autoFocus={true}
      />
      <Input.Password
        value={password}
        placeholder="Password"
        onChange={event => {
          setPassword(event.target.value);
        }}
        name="password"
        status={viewer?.errors?.password ? 'error' : ''}
      />
      <Button
        type="primary"
        onClick={handlerSignIn}
        disabled={viewer?.loading || !email || !password}
        loading={viewer?.loading}
      >
        Log In
      </Button>
      <Link onClick={() => navigate(navigationMap.register)}>Create new account?</Link>
    </div>
  );
};
