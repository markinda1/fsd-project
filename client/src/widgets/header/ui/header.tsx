import './header.scss';
import { Button } from 'antd';
import clsx from 'clsx';
import * as React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch } from '@/app/appStore.ts';
import { logOut, useAuth } from '@/entities/viewer';
import { navigationMap } from '@/shared/model';
import { Logo } from '@/shared/ui';

export const Header = () => {
  const dispatch = useAppDispatch();
  const isAuth = useAuth();
  const navigate = useNavigate();

  const handlerLogin = React.useCallback(() => {
    navigate(navigationMap.login);
  }, [navigate]);

  const handlerLogout = React.useCallback(() => {
    dispatch(logOut());
  }, [dispatch]);

  return (
    <header className={clsx('app-header')}>
      <div className={clsx('app-header__container', 'container')}>
        <div className={clsx('app-header__item')}>
          <Logo />
        </div>
        <div className={clsx('app-header__item')}>
          <nav className={clsx('app-nav', 'app-header__nav')}>
            <div className={clsx('app-nav__item')}>
              <Link to={navigationMap.main}>Home</Link>
              {/*<Link href={navigationMap.main}>Home</Link>*/}
            </div>
            <div className={clsx('app-nav__item')}>
              <Link to={navigationMap.profile}>Profile</Link>
              {/*<Link href={navigationMap.profile}>Profile</Link>*/}
            </div>
          </nav>
        </div>
        <div className={clsx('app-header__item')}>
          {isAuth ? (
            <Button danger onClick={handlerLogout} className={clsx('app-header__log-in')}>
              Log Out
            </Button>
          ) : (
            <Button onClick={handlerLogin} className={clsx('app-header__log-in')}>
              Log In
            </Button>
          )}
        </div>
      </div>
    </header>
  );
};
