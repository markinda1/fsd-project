import clsx from 'clsx';
import { Link } from 'react-router-dom';
import { navigationMap } from '@/shared/model';
import './logo.scss';

export const Logo = () => {
  return (
    <Link className={clsx('logo')} to={navigationMap.main}>
      <span className={clsx('logo__text')}>FSD</span>
    </Link>
  );
};
