import { createBrowserHistory } from 'history';

export const appHistory: any = createBrowserHistory({ window });
