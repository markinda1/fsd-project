export const userNoPhoto = (name: string) => {
  return name
    .split(' ')
    .map((item: string) => {
      return item[0];
    })
    .join('');
};
