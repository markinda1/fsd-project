import { ROLE } from '@/shared/model';

export interface ILogInData {
  email: string;
  password: string;
}

export interface ILogInResponse {
  accessToken?: string;
  errors?: Record<string, string>;
  message?: string;
}

export interface IMeResponse {
  id: number;
  email: string;
  role: ROLE;
  name: string;
}

export interface IRefreshTokenResponse {
  accessToken?: string;
}
