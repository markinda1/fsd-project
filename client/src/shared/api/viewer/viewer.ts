import { Api } from '../base';
import { ILogInData, ILogInResponse, IMeResponse, IRefreshTokenResponse } from './types';

export const apiLogIn = async (data: ILogInData) => await Api.post<ILogInResponse>('auth/login', data);

export const apiLogOut = async () => await Api.get('auth/logout');

export const apiGetMe = async () => await Api.get<IMeResponse>('auth/me');

export const apiRefreshToken = async () => await Api.get<IRefreshTokenResponse>('auth/refresh');
