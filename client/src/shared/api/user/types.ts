import { ROLE } from '@/shared/model';

export interface IUserCreateData {
  email: string;
  name: string;
  password: string;
  passwordRepeat: string;
}

export interface IUser {
  id: number;
  name: string;
  email: string;
  role: ROLE;
}

export interface IUserCreateResponse {
  data?: IUser;
  errors?: Record<string, string>;
  message?: string;
}
