import { Api } from '../base.ts';
import { IUserCreateData, IUserCreateResponse } from './types';

export const apiUserCreate = async (data: IUserCreateData) => await Api.post<IUserCreateResponse>('user', data);
