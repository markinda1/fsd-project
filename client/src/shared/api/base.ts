import axios from 'axios';

export const Api = axios.create({
  baseURL: 'http://localhost:3002/api/',
});

Api.interceptors.request.use(
  function (config) {
    config.headers.Authorization = window.localStorage.getItem('accessToken');
    return config;
  },
  function (error) {
    return error.response;
  },
);

Api.interceptors.response.use(
  function (config) {
    return config;
  },
  function (error) {
    return error.response;
  },
);
