export const navigationMap = {
  main: '/',
  login: '/login',
  register: '/register',
  profile: '/profile',
  notFound: '/not-found',
};
