import { createSelector } from '@reduxjs/toolkit';
import { useAppSelector } from '@/app/appStore.ts';
import { usersAdapter } from './slice.ts';

export const useUsers = () => {
  return useAppSelector(store => store.users);
};

const usersSelectors = usersAdapter.getSelectors<RootState>(state => state.users);

export const selectUsers = createSelector(
  (state: RootState) => usersSelectors.selectAll(state),
  users => users,
);
