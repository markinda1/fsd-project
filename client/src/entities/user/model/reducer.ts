import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiUserCreate, IUserCreateData } from '@/shared/api';
import { appHistory } from '@/shared/lib';
import { navigationMap } from '@/shared/model';

export const userCreate = createAsyncThunk('users/create', async (data: IUserCreateData, { rejectWithValue }) => {
  const response = await apiUserCreate(data);

  if (Object.values(response.data?.errors || {})?.length || response.data.message) {
    return rejectWithValue(response.data?.errors || response.data);
  } else {
    appHistory.push(navigationMap.login);
  }

  return response.data;
});
