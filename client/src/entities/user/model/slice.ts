import { createEntityAdapter, createSlice, EntityId } from '@reduxjs/toolkit';
import { IUser } from '@/shared/api';
import { userCreate } from './reducer.ts';
import { IUsersState } from './types.ts';

export const usersAdapter = createEntityAdapter<IUser, EntityId>({
  selectId: state => state.id,
});

const initialState: IUsersState = {
  ...usersAdapter.getInitialState({ loading: false, errors: {} }),
};

export const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    // standard reducer logic, with auto-generated action types per reducer
  },
  extraReducers: builder => {
    builder.addCase(userCreate.pending, state => {
      state.loading = true;
      state.errors = {};
    });
    builder.addCase(userCreate.fulfilled, (state, action) => {
      state.loading = false;
      state.errors = {};
      usersAdapter.addOne(state, action.payload.data as IUser);
    });
    builder.addCase(userCreate.rejected, (state, action) => {
      state.loading = false;
      state.errors = action.payload as Record<string, string>;
    });
  },
});
