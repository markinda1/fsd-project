import { EntityId } from '@reduxjs/toolkit';
import { IUser } from '@/shared/api';

export interface IUsersState {
  loading: boolean;
  ids: EntityId[];
  entities: Record<EntityId, IUser>;
  errors: Record<string, string>;
}
