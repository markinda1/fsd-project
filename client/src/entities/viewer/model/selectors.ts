import { useAppSelector } from '@/app/appStore.ts';

export const useViewer = () => {
  return useAppSelector(store => store.viewer);
};
export const useAuth = () => {
  const viewer = useViewer();
  return !!viewer?.auth.accessToken;
};
