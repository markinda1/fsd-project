import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiGetMe, apiLogIn, apiLogOut, apiRefreshToken, ILogInData } from '@/shared/api';
import { removeToken, setToken } from '@/shared/lib';

export const signIn = createAsyncThunk('signIn', async (data: ILogInData, { rejectWithValue }) => {
  const response = await apiLogIn(data);

  if (Object.values(response.data?.errors || {})?.length || response.data.message) {
    return rejectWithValue(response.data?.errors || response.data);
  }

  if (response?.data?.accessToken) {
    setToken(response?.data?.accessToken);
  }

  return response.data;
});

export const getMe = createAsyncThunk('me', async (token: string) => {
  const response = await apiGetMe().then(async res => {
    if (res.status === 401) {
      const response = await apiRefreshToken().then(res => {
        if (res.status === 401) {
          removeToken();
        }

        return res;
      });

      if (response?.data?.accessToken) {
        setToken(response?.data?.accessToken);
      }
    }

    return res;
  });

  return {
    ...response.data,
    accessToken: token,
  };
});

export const logOut = createAsyncThunk('logOut', async () => {
  const response = await apiLogOut();

  if (response.data) {
    removeToken();
  }
});
