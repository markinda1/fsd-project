import { createSlice } from '@reduxjs/toolkit';
import { ROLE } from '@/shared/model';
import { getMe, logOut, signIn } from './reducers.ts';
import { IViewerState } from './types.ts';

const initialState: IViewerState = {
  loading: false,
  auth: {
    id: 0,
    name: '',
    email: '',
    role: ROLE.VIEW,
    accessToken: null,
  },
  errors: {},
};

export const viewerSlice = createSlice({
  name: 'viewer',
  initialState,
  reducers: {
    // standard reducer logic, with auto-generated action types per reducer
  },
  extraReducers: builder => {
    builder.addCase(signIn.pending, state => {
      state.loading = true;
      state.errors = {};
    });
    builder.addCase(signIn.fulfilled, (state, action) => {
      state.loading = false;
      state.errors = {};
      state.auth.accessToken = action.payload?.accessToken || null;
    });
    builder.addCase(signIn.rejected, (state, action) => {
      state.loading = false;
      state.errors = action.payload as Record<string, string>;
    });

    builder.addCase(logOut.pending, state => {
      state.loading = true;
    });
    builder.addCase(logOut.fulfilled, state => {
      state.loading = false;
      state.auth = {
        ...initialState.auth,
      };
    });
    builder.addCase(logOut.rejected, state => {
      state.loading = false;
    });

    builder.addCase(getMe.pending, state => {
      state.loading = true;
    });
    builder.addCase(getMe.fulfilled, (state, action) => {
      state.loading = false;
      state.auth = action.payload;
    });
    builder.addCase(getMe.rejected, state => {
      state.loading = false;
    });
  },
});
