import { ROLE } from '@/shared/model';

export interface IViewer {
  id: number;
  name: string;
  role: ROLE;
  email: string;
  accessToken: string | null;
}

export interface IViewerState {
  loading: boolean;
  auth: IViewer;
  errors: Record<string, string>;
}
