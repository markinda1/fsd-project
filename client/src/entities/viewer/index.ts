export { viewerSlice, useViewer, useAuth, signIn, getMe, logOut, type IViewer } from './model';
